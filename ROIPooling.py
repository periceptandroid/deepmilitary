from keras.engine.topology import Layer
import keras.backend as K
import tensorflow as tf
import numpy
import sys


class ROIPoolingLayer(Layer):

    '''
    ROI pooling layer for 2D inputs.
   
    # Input shape
        list of two 4D tensors [X_features,X_roi] with shape:
        
        X_features = [:,11,20,512]
        X_roi = [:,11,20,4], coordinate modifiers
        
    # Output shape

        4D tensor with shape [:,11,20,512]
        
    '''
    def __init__(self,   **kwargs):
        super(ROIPoolingLayer, self).__init__(**kwargs)



    def compute_output_shape(self, input_shape):
        
        feature_map_shape, rois_shape = input_shape

        return (rois_shape[0], rois_shape[1], rois_shape[2], feature_map_shape[3])



    def call(self, x, mask=None):

        #pull out the features and ROI inputs
        feature_map = x[0]
        rois = x[1]

   
        def total(input): 

            #and peel off the current element in the batch
            local_feature_map = input[0]
            local_rois = input[1]

            #how big are we
            h = K.shape(local_feature_map)[0]
            w = K.shape(local_feature_map)[1]
        
            #and we'll need this to maintain anchor coordinates
            #through the mapfns
            X,Y = tf.meshgrid(tf.range(w), tf.range(h))
            X = K.cast(X, 'float32')
            Y = K.cast(Y, 'float32')

            def stage2_anchor_columns(input): 
                return ROIPoolingLayer._pool_roi(local_feature_map, input[0],input[1],input[2])

            def stage1_anchor_rows(input): 
                return tf.map_fn(stage2_anchor_columns, (input[0], input[1],input[2]), dtype=tf.float32) 
            
            #mapfn over the anchor space, rows then columns    
            local_pooled_areas = tf.map_fn(stage1_anchor_rows, (local_rois, X,Y) , dtype=tf.float32)
            return local_pooled_areas

        #start by map_fning each element of the batch
        pooled_areas = tf.map_fn(total, (feature_map, rois), dtype=tf.float32)
            
        return pooled_areas

        
      
    #so given a 11x20x512 feature_map ... and a 11x20x4 roi ... 
    #pool down the features for the region at the [rj,ri]th anchor
    @staticmethod
    def _pool_roi(feature_map, roi,ri,rj):

        #size of the feature map
        h = K.shape(feature_map)[0]
        w = K.shape(feature_map)[1]

        #raw anchor coordinates in anchor space
        x1_anc = ri 
        y1_anc = rj
        x2_anc = ri + 1
        y2_anc = rj + 1 

        #regressed coordinate modifiers
        tx = roi[ 0]
        ty = roi[ 1]
        tw = roi[ 2]
        th = roi[ 3]

        cx = (x1_anc + x2_anc)/2
        cy = (y1_anc + y2_anc)/2
        cw = (x2_anc - x1_anc)
        ch = (y2_anc - y1_anc)

        #apply the modifiers
        cx = cx + tx * cw
        cy = cy + ty * ch
        cw = K.exp(tw) * cw
        ch = K.exp(th) * ch

        #and cast those to coordinates
        x1 = K.cast(tf.math.floor(cx - cw/2), 'int32')
        x2 = K.cast(tf.math.ceil(cx + cw/2), 'int32')
        y1 = K.cast(tf.math.floor(cy - ch/2), 'int32')
        y2 = K.cast(tf.math.ceil(cy + ch/2), 'int32')

        x1 = K.maximum(1,x1)
        y1 = K.maximum(1,y1)
        x2 = K.minimum(w-1,x1)
        y2 = K.minimum(h-1,y1)
        x2 = x1 + K.maximum(1,x2-x1)
        y2 = y1 + K.maximum(1,y2-y1)

        x1 = K.cast(x1_anc, 'int32')
        y1 = K.cast(y1_anc, 'int32')
        x2 = K.cast(x2_anc, 'int32')
        y2 = K.cast(y2_anc, 'int32')
     
        #and pool down the feature map in that box
        rs = tf.math.reduce_mean(feature_map[ y1:y2+1,x1:x2+1, :], axis=[0,1])
        
        return rs

    #I have never used this ...
    def get_config(self):
        config = {}
        base_config = super(ROIPoolingLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))