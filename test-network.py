#obvious stuff
import cv2
import numpy as np
import os

import math
import random

import tensorflow as tf
from tensorflow import keras

#import keras stuff ... has been accumulating over files :)
from keras import backend as K
from keras.optimizers import Adam, SGD, RMSprop
from keras.layers import Flatten, Dense, Input, Conv2D, MaxPooling2D, Dropout
from keras.layers import GlobalAveragePooling2D, GlobalMaxPooling2D, TimeDistributed, Concatenate
from keras.engine.topology import get_source_inputs
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras.objectives import categorical_crossentropy
from keras.callbacks import ModelCheckpoint
from keras.models import Model
from keras.utils import generic_utils
from keras.engine import Layer, InputSpec
from keras import initializers, regularizers
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array

#import keras VGG models
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input


#and our custom ROIclassLabelsing layer
from ROIPooling import ROIPoolingLayer


epsilon = 1e-4

#what we will rescale our input images to
TARGET_IMAGE_SIZE_WIDTH = 320
TARGET_IMAGE_SIZE_HEIGHT = 180

#pixel-width of our anchor boxes
RPN_STRIDE_WIDTH = 16
RPN_STRIDE_HEIGHT = 16

NUM_CLASSES = 4

#union/intersection utilities
def union(au, bu, areaIntersection):
	areaA = (au[2] - au[0]) * (au[3] - au[1])
	areaB = (bu[2] - bu[0]) * (bu[3] - bu[1])
	areaUnion = areaA + areaB - areaIntersection
	return areaUnion


def intersection(ai, bi):
	x = max(ai[0], bi[0])
	y = max(ai[1], bi[1])
	w = min(ai[2], bi[2]) - x
	h = min(ai[3], bi[3]) - y
	if w < 0 or h < 0:
		return 0
	return w*h


def iou(a, b):
    
    # a and b should be (x1,y1,x2,y2)    	
	if a[0] >= a[2] or a[1] >= a[3] or b[0] >= b[2] or b[1] >= b[3]:
		return 0.0

	area_i = intersection(a, b)
	area_u = union(a, b, area_i)

	return float(area_i) / float(area_u + 1e-6)




#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Network definition
#################################################################################
#################################################################################
#################################################################################
#################################################################################


#network definition
def layerRPN(input_features):
 
    x = Conv2D(1024, (1, 1),  activation='relu', name='rpn_conv1')(input_features)
    x = Conv2D(512, (1, 1), activation='relu', name='rpn_conv2')(x)
    x = Dropout(0.15, name='rpn_dropout')(x)
    x = Conv2D(512, (1, 1),  activation='relu', name='rpn_conv3')(x)
    x = Conv2D(2048, (1, 1),  activation='relu', name='rpn_conv4')(x)

    objectness = Conv2D(1, (1, 1), activation='sigmoid', name='rpn_out_objectness')(x)
    boxcoords = Conv2D(4, (1, 1), activation='linear', name='rpn_out_boxcoords')(x)

    return [objectness, boxcoords]

def lossRPNRegression():

    #y_true[:,:,:,:4] is either all 1 if we should count this anchor and 0 otherwise

    #y_true[:,:,:,4:] is the regression targets

    def _lossRPNRegression(y_true, y_pred):
        
        diff = y_true[:,:,:,4:] - y_pred
        absDiff = K.abs(diff)

        isClose = K.cast(K.less_equal(absDiff, 1.0), tf.float32)

        return K.sum( y_true[:,:,:,:4] * (isClose * (0.5 * diff * diff) + (1 - isClose) * (absDiff - 0.5))) / K.sum(epsilon + y_true[:,:,:,:4])

    return _lossRPNRegression


def lossRPNObjectness():
  
    #y_true[:,:,:,:1] ... whether this anchor is should count here
    #y_true[:,:,:,1:] ... the 0/1 positive negative flag of the anchor

    def _lossRPNObjectness(y_true, y_pred):
            
            dx = y_pred[:,:,:,:] - y_true[:,:,:,1:]
            mask = y_true[:,:,:,:1]
            value = y_true[:,:,:,1:]
            
            #weighted means-square error
            weight = mask * ( value * 0.95 + (1-value) * 0.05)            
            return  K.sum(weight * dx * dx) / K.sum(epsilon + y_true[:,:,:,:1])

    return _lossRPNObjectness


#anchor classIdx layer
def layerClassification(baseLayers, inputObjectness, inputCoords):
  
    pooling = ROIPoolingLayer()([baseLayers, inputCoords])
    
    x = Conv2D(256, (1, 1), padding='same', activation='relu',  name='class_conv1')(pooling)
    x = Dropout(0.15, name='class_dropout')(x)
    x = Conv2D(64, (1, 1), padding='same', activation='relu',  name='class_conv2')(x)

    outputClass = Conv2D(4, (1, 1),  activation='softmax',  name='class_out')(x)

    return [inputObjectness, inputCoords,   outputClass]

#CCE loss for classification
def lossClassification():

    def _lossClassification(y_true, y_pred):
            
            mask = y_true[:, :, :, 0] #whether this was associated with a valid anchor
            value = y_true[:, :, :, 1:] #the one-hot class
            
            error = categorical_crossentropy(y_pred, value) * mask

            return  K.mean(error)
            
    return _lossClassification



#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Non-maximum supression ... just generic copy/paste
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################


def non_max_suppression_fast(boxes, classes, probs, overlap_thresh=0.9, max_boxes=300):
    
    # code used from here: http://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
    # if there are no boxes, return an empty list

    # Process explanation:
    #   Step 1: Sort the probs list
    #   Step 2: Find the larget prob 'Last' in the list and save it to the pick list
    #   Step 3: Calculate the IoU with 'Last' box and other boxes in the list. If the IoU is larger than overlap_threshold, delete the box from list
    #   Step 4: Repeat step 2 and step 3 until there is no item in the probs list 
    if len(boxes) == 0:
        return []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0] 
    y1 = boxes[:, 1] 
    x2 = boxes[:, 2] 
    y2 = boxes[:, 3] 

    np.testing.assert_array_less(x1, x2)
    np.testing.assert_array_less(y1, y2)

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    classes = np.array(classes)

    # initialize the list of picked indexes	
    pick = []

    # calculate the areas
    area = (x2 - x1) * (y2 - y1)

    # sort the bounding boxes 
    idxs = np.argsort(probs)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the intersection

        xx1_int = np.maximum(x1[i], x1[idxs[:last]])
        yy1_int = np.maximum(y1[i], y1[idxs[:last]])
        xx2_int = np.minimum(x2[i], x2[idxs[:last]])
        yy2_int = np.minimum(y2[i], y2[idxs[:last]])

        ww_int = np.maximum(0, xx2_int - xx1_int)
        hh_int = np.maximum(0, yy2_int - yy1_int)

        area_int = ww_int * hh_int

        # find the union
        area_union = area[i] + area[idxs[:last]] - area_int

        # compute the ratio of overlap
        overlap = area_int/(area_union + 1e-6)

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > overlap_thresh)[0])))

        if len(pick) >= max_boxes:
            break

    # return only the bounding boxes that were picked using the integer data type
    boxes = boxes[pick].astype("int")
    classes = classes[pick]
    probs = probs[pick]
    return boxes, classes, probs



#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Main program
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################


#don't blow up the computer
opts = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.7)
sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=opts))


# weight_file = r'vgg16_weights_tf_dim_ordering_tf_kernels.h5'
modelFile  = r'model.hdf5'

inputImageShape = (180,320,3)
inputImage = Input(shape=inputImageShape)

#load VGG 
model = VGG16(include_top=False, input_tensor=inputImage, pooling='avg')
model.layers.pop()
model.layers.pop()
for l in model.layers:
    l.trainable = False
    

rpn = layerRPN(model.layers[-1].output)
classification = layerClassification(model.layers[-1].output, rpn[0], rpn[1])

totalModel = Model(inputImage,classification)
totalModel.summary()

optimizer = Adam()
totalModel.compile(optimizer=optimizer, loss=[lossRPNObjectness(), lossRPNRegression(), lossClassification()])
totalModel.load_weights(modelFile, by_name=True)


    
#load the video to test on
cap = cv2.VideoCapture(r"test.mp4")

#and the video to write the debug renders to
out = cv2.VideoWriter('outputx.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 30, (320,180))

#and we'll save the raw debug renders in a directory
OUTPUT_DIR = "detections"
os.system("mkdir " + OUTPUT_DIR)
    

#keep track of frame index
frameCount = 0

#until we error out, don't care
while True:
    
    #read the frame and write the current frame to a temp file for viewing
    ret, frame = cap.read()
    cv2.imwrite("temp.bmp", frame)
    
    #and load the file for processing
    imgInput = load_img("temp.bmp", target_size=(180, 320))
    imgInput = img_to_array(imgInput)

    #and also copy it so we can mark it up for rendering purposes
    imgAug = imgInput.copy().astype('uint8')
    imgAug = cv2.cvtColor(imgAug, cv2.COLOR_RGB2BGR)

    #we have the dims ... but makes me feel better to explicitly get them
    h,w = imgInput.shape[:2]
    
    #prep the input for TF
    imgInput = np.expand_dims(imgInput, axis=0)
    imgInput = preprocess_input(imgInput)

    modelOutput = totalModel.predict_on_batch(imgInput)


    print("Objectness shape",modelOutput[0].shape)
    print("Coords shape",modelOutput[1].shape)
    print("Classification shape",modelOutput[2].shape)


    #for the non-max supression
    bboxes_for_cull = []
    probs_for_cull = []
    class_for_cull = []

    #show the predictions
    rheight = int(TARGET_IMAGE_SIZE_HEIGHT/RPN_STRIDE_HEIGHT)
    rwidth = int(TARGET_IMAGE_SIZE_WIDTH/RPN_STRIDE_WIDTH)
    
    for jy in range(rheight):
        for ix in range(rwidth):
            
            anchor_width = RPN_STRIDE_WIDTH
            anchor_height = RPN_STRIDE_HEIGHT
            
            overlap = modelOutput[0][0,jy,ix,0]
            coordsRegression = modelOutput[1][0,jy,ix,:]
            
            tx = coordsRegression[0]
            ty = coordsRegression[1]
            tw = coordsRegression[2]
            th = coordsRegression[3]
        
            #if this is a high objectness score
            if overlap > 0.9:
                
                x1_anc = int(RPN_STRIDE_WIDTH * (float(ix)) )
                x2_anc = int(RPN_STRIDE_WIDTH * (float(ix) + 1) )
                y1_anc = int(RPN_STRIDE_HEIGHT * (float(jy)) )
                y2_anc = int(RPN_STRIDE_HEIGHT * (float(jy) + 1) )

                cx = (x1_anc + x2_anc)/2
                cy = (y1_anc + y2_anc)/2
                cw = (x2_anc - x1_anc)
                ch = (y2_anc - y1_anc)

                cx = cx + tx * cw
                cy = cy + ty * ch
                cw = np.exp(tw) * cw
                ch = np.exp(th) * ch

                nx1 = int((cx - cw/2))
                nx2 = int((cx + cw/2))
                ny1 = int((cy - ch/2))
                ny2 = int((cy + ch/2))

                classIdx = np.argmax(modelOutput[2][0,jy,ix,:])

                #if this is a reasonable box ... then add it to the non-max list
                if (nx1 > 0 and ny1 > 0 and nx2 < 320 and ny2 < 180):

                    bboxes_for_cull.append([nx1,ny1,nx2,ny2])
                    probs_for_cull.append(overlap)
                    class_for_cull.append(classIdx)

                    # if (classIdx == 1):
                    #     imgAug = cv2.rectangle(imgAug, (nx1,ny1), (nx2,ny2), (255,0,0), 1)
                    # elif (classIdx == 2):
                    #     imgAug = cv2.rectangle(imgAug, (nx1,ny1), (nx2,ny2), (0,255,0), 1)
                    # elif (classIdx == 3):
                    #     imgAug = cv2.rectangle(imgAug, (nx1,ny1), (nx2,ny2), (0,0,255), 1)


    #and if we HAVE something to display
    if len(bboxes_for_cull) > 0:

        #do the non-max supression
        bboxes_for_cull = np.vstack(bboxes_for_cull)
        probs_for_cull = np.hstack(probs_for_cull)

        cull_box, cull_class, cull_prob = non_max_suppression_fast(bboxes_for_cull, class_for_cull, probs_for_cull, overlap_thresh=0.0, max_boxes=300)

        for b in range(cull_box.shape[0]):

            x1 = cull_box[b,0] 
            y1 = cull_box[b,1] 
            x2 = cull_box[b,2] 
            y2 = cull_box[b,3] 

            classIdx = cull_class[b]

            #draw the final boxes
            if (classIdx == 1):
                imgAug = cv2.rectangle(imgAug, (x1,y1), (x2,y2), (255,0,0), 2)
            elif (classIdx == 2):
                imgAug = cv2.rectangle(imgAug, (x1,y1), (x2,y2), (0,255,0), 2)
            elif (classIdx == 3):
                imgAug = cv2.rectangle(imgAug, (x1,y1), (x2,y2), (0,0,255), 2)
                            
    
    cv2.imshow("testing", imgAug)    
    out.write(imgAug)
    frameCount += 1
    cv2.imwrite(OUTPUT_DIR + "\\frame" + '{:05}'.format(frameCount) + ".bmp", imgAug)
    
    cv2.waitKey(1)

