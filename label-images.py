import cv2
import numpy as np
import os

#utility for converting video offset ms to standard
#timestamp string
def timeString(ms):
    return '{:013}'.format(int(ms))

#we'll need two frames ... a raw video frame and a frame
#we draw things on top of ... for now we just need placeholders
rawFrame = np.zeros((10,10,3), dtype=np.uint8)
markedFrame = np.zeros((10,10,3), dtype=np.uint8)

#we'll store the timestamp of the current frame here
timestamp = 0

#we'll store the rectangles of the labels here
aircraft = []
ships = []
people = []


#we'll store all the annotations in separate directories
IMAGES_DIRECTORY = "images"
AIRCRAFT_DIRECTORY = "aircraft"
SHIPS_DIRECTORY = "ships"
PEOPLE_DIRECTORY = "people"

#make sure we HAVE the directories to work with
os.system("mkdir " + IMAGES_DIRECTORY) #this should be here otherwise we have no source material
os.system("mkdir " + AIRCRAFT_DIRECTORY)
os.system("mkdir " + SHIPS_DIRECTORY)
os.system("mkdir " + PEOPLE_DIRECTORY)


#for a given frame (indexed by timestamp)
#load the existing annotations from the file names
#in the specified directory
def loadAnnotations(directory, timestamp):

    annotations = []

    #iterate through the directory
    for imgFile in os.listdir(directory):

        #and split off the file name into parts
        baseName = os.path.splitext(imgFile)[0]
        annotationParts = baseName.split('_')

        #if the first part of the 
        if (timestamp != annotationParts[0]):
            continue
                
        #this is how we've chosen to encode
        #the annotation coordinates
        x1 = int(int(annotationParts[1]))
        y1 = int(int(annotationParts[2]))
        x2 = int(int(annotationParts[3]))
        y2 = int(int(annotationParts[4]))
        
        #and save it!
        annotations.append((x1,y1,x2-x1,y2-y1))

    return annotations

#here we markup the raw frame with all the graphics
#we want to display to the user
def annotateRawFrame():

    markedFrame = rawFrame.copy()

    h,w = markedFrame.shape[:2]
    markedFrame = cv2.putText(markedFrame, timeString(timestamp), (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    
    markedFrame = cv2.putText(markedFrame, "A: label aircraft", (10,h-10), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    markedFrame = cv2.putText(markedFrame, "S: label ship", (10,h-40), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    markedFrame = cv2.putText(markedFrame, "D: label person", (10,h-70), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    markedFrame = cv2.putText(markedFrame, "T: save labels", (10,h-100), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    markedFrame = cv2.putText(markedFrame, "N: next frame", (10,h-130), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)

    #draw the aircraft annotations
    for r in aircraft:
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        cx = int((x1 + x2)/2)
        cy = int((y1 + y2)/2)
        markedFrame = cv2.rectangle(markedFrame, (x1,y1), (x2,y2), (0,255,0) )
        markedFrame = cv2.putText(markedFrame, "AIRCRAFT", (cx,cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2, cv2.LINE_4)

    #draw the ship annotations
    for r in ships:
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        cx = int((x1 + x2)/2)
        cy = int((y1 + y2)/2)
        markedFrame = cv2.rectangle(markedFrame, (x1,y1), (x2,y2), (255,255,0) )
        markedFrame = cv2.putText(markedFrame, "SHIP", (cx,cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0), 2, cv2.LINE_4)

    #draw the people annotations
    for r in people:
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        cx = int((x1 + x2)/2)
        cy = int((y1 + y2)/2)
        markedFrame = cv2.rectangle(markedFrame, (x1,y1), (x2,y2), (0,255,255) )
        markedFrame = cv2.putText(markedFrame, "PERSON", (cx,cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,255), 2, cv2.LINE_4)

    return markedFrame


#crop and save all the annotations ... each file will become 
#a different annotation ... since files contain the timestamp and
#coordinates of the label, the filename has all the information we
#need for the label
def saveAnnotations(timestamp):

    #save all the aircraft annotations
    for i,r in enumerate(aircraft):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]

        #we just use a simple _ delimited filename to encode the coordinates
        cv2.imwrite(os.path.join(AIRCRAFT_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)

    #now save all the ship annotations
    for i,r in enumerate(ships):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]
        cv2.imwrite(os.path.join(SHIPS_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)

    #save all the person annotations
    for i,r in enumerate(people):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]
        cv2.imwrite(os.path.join(PEOPLE_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)









#starting off we'll load all the pre-selected
#images so we can iterate over them and annotate
timestamps = []
for imgFile in os.listdir(IMAGES_DIRECTORY):

    baseName = os.path.splitext(imgFile)[0]
    timestamps.append(baseName)

#sort the timestamps based on filename since 
#they might not have been read in order from the OS
timestamps.sort()

#and start at the top of the stack
frameCount = 0


#load the first image and its annotations
timestamp = timestamps[frameCount]
rawFrame = cv2.imread(os.path.join(IMAGES_DIRECTORY, timestamp + ".bmp"))
aircraft = loadAnnotations(AIRCRAFT_DIRECTORY, timestamp)
people = loadAnnotations(PEOPLE_DIRECTORY, timestamp)
ships = loadAnnotations(SHIPS_DIRECTORY, timestamp)

while True:

    #draw the existing annotations on the frame
    markedFrame = annotateRawFrame()
    cv2.imshow("Annotation Tool", markedFrame)

    #get user input
    key = cv2.waitKey(0) & 0xFF

    #quit
    if key == ord("q"):
        exit()

    #request to label an aircraft region
    elif key == ord("a"): #aircraft

        #get the box from the user
        r = cv2.selectROI("Annotation Tool", markedFrame, showCrosshair=True, fromCenter=False)
        if (r[2] != 0 and r[3] != 0):
            aircraft.append(r)

    #label a ship
    elif key == ord("s"):
        r = cv2.selectROI("Annotation Tool", markedFrame, showCrosshair=True, fromCenter=False)
        if (r[2] != 0 and r[3] != 0):
            ships.append(r)

    #label a person
    elif key == ord("d"): 
        r = cv2.selectROI("Annotation Tool", markedFrame, showCrosshair=True, fromCenter=False)
        if (r[2] != 0 and r[3] != 0):
           people.append(r)
 
    #if the user goes on to the next frame
    #for convenience, we forbid progression if there are no annotations on the frame    
    elif key == ord("n") and (len(ships) > 0 or len(people) > 0 or len(aircraft) > 0):
        
        frameCount += 1
        timestamp = timestamps[frameCount]
        rawFrame = cv2.imread(os.path.join(IMAGES_DIRECTORY, timestamp + ".bmp"))
        aircraft = loadAnnotations(AIRCRAFT_DIRECTORY, timestamp)
        people = loadAnnotations(PEOPLE_DIRECTORY, timestamp)
        ships = loadAnnotations(SHIPS_DIRECTORY, timestamp)
       
    #create a label/annotation file for each label
    elif key == ord("t"):
        saveAnnotations(timestamp)

    print("Current timestamp:",timestamp)
   
    
    
    
        
         