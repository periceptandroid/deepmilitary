#obvious stuff
import cv2
import numpy as np
import os

import math
import random

import tensorflow as tf
from tensorflow import keras

#import keras stuff ... has been accumulating over files :)
from keras import backend as K
from keras.optimizers import Adam, SGD, RMSprop
from keras.layers import Flatten, Dense, Input, Conv2D, MaxPooling2D, Dropout
from keras.layers import GlobalAveragePooling2D, GlobalMaxPooling2D, TimeDistributed, Concatenate
from keras.engine.topology import get_source_inputs
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras.objectives import categorical_crossentropy
from keras.callbacks import ModelCheckpoint
from keras.models import Model
from keras.utils import generic_utils
from keras.engine import Layer, InputSpec
from keras import initializers, regularizers
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array

#import keras VGG models
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input


#and our custom ROIclassLabelsing layer
from ROIPooling import ROIPoolingLayer


epsilon = 1e-4

#what we will rescale our input images to
TARGET_IMAGE_SIZE_WIDTH = 320
TARGET_IMAGE_SIZE_HEIGHT = 180

#pixel-width of our anchor boxes
RPN_STRIDE_WIDTH = 16
RPN_STRIDE_HEIGHT = 16

NUM_CLASSES = 4


#union/intersection utilities
def union(au, bu, areaIntersection):
	areaA = (au[2] - au[0]) * (au[3] - au[1])
	areaB = (bu[2] - bu[0]) * (bu[3] - bu[1])
	areaUnion = areaA + areaB - areaIntersection
	return areaUnion


def intersection(ai, bi):
	x = max(ai[0], bi[0])
	y = max(ai[1], bi[1])
	w = min(ai[2], bi[2]) - x
	h = min(ai[3], bi[3]) - y
	if w < 0 or h < 0:
		return 0
	return w*h


def iou(a, b):
    
    # a and b should be (x1,y1,x2,y2)    	
	if a[0] >= a[2] or a[1] >= a[3] or b[0] >= b[2] or b[1] >= b[3]:
		return 0.0

	area_i = intersection(a, b)
	area_u = union(a, b, area_i)

	return float(area_i) / float(area_u + 1e-6)



#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Assign the anchor to bounding boxes
#################################################################################
#################################################################################
#################################################################################
#################################################################################

def boxesToAnchors(  boxCoords, boxClasses ):
    
    #determine how big our output texture should be
    outputWidth = int(TARGET_IMAGE_SIZE_WIDTH / RPN_STRIDE_WIDTH)
    outputHeight = int(TARGET_IMAGE_SIZE_HEIGHT / RPN_STRIDE_HEIGHT)
        
    #allocate our output textures
    anchorOverlap = np.zeros((outputHeight, outputWidth, 1))
    anchorValid = np.zeros((outputHeight, outputWidth, 1))
    anchorCoords = np.zeros((outputHeight, outputWidth,  4))
    anchorClass = np.zeros((outputHeight, outputWidth, NUM_CLASSES))

    numBoxes = len(boxCoords)

    #for every possible anchor position
    for ix in range(outputWidth):					
        
        # pixel x-coordinates of the anchor
        x1_anc = int(RPN_STRIDE_WIDTH * (float(ix) + 0.5) - RPN_STRIDE_WIDTH / 2)
        x2_anc = int(RPN_STRIDE_WIDTH * (float(ix) + 0.5) + RPN_STRIDE_WIDTH / 2)
        if x1_anc < 0 or x2_anc > TARGET_IMAGE_SIZE_WIDTH:
            continue
                
        for jy in range(outputHeight):

            # pixel y-coordinates of the anchor
            y1_anc = int(RPN_STRIDE_HEIGHT * (float(jy) + 0.5) - RPN_STRIDE_HEIGHT / 2)
            y2_anc = int(RPN_STRIDE_HEIGHT * (float(jy) + 0.5) + RPN_STRIDE_HEIGHT / 2)
            if y1_anc < 0 or y2_anc > TARGET_IMAGE_SIZE_HEIGHT:
                continue
            
            #by default set the class to no-object
            anchorClass[jy,ix,:] = [1,0,0,0]

            #by default this anchor is set to not valid 
            anchorType = 'no-object'

            #now see if any box touches this anchor
            for boxIdx in range(numBoxes):            
                
                #some box stats
                boxWidth = (boxCoords[boxIdx][2] - boxCoords[boxIdx][0])
                boxHeight = (boxCoords[boxIdx][3] - boxCoords[boxIdx][1]) 
                boxArea = boxWidth * boxHeight

                #and see if the anchor touches the bounding box
                anchorArea = (x2_anc - x1_anc) * (y2_anc - y1_anc) 
                anchorIOU = intersection([boxCoords[boxIdx][0], boxCoords[boxIdx][1], boxCoords[boxIdx][2], boxCoords[boxIdx][3]], [x1_anc, y1_anc, x2_anc, y2_anc]) / anchorArea

  
                #compare the center of the anchor with the center of the bounding box
                cx = (boxCoords[boxIdx][0] + boxCoords[boxIdx][2]) / 2.0
                cy = (boxCoords[boxIdx][1] + boxCoords[boxIdx][3]) / 2.0
                cxa = (x1_anc + x2_anc)/2.0
                cya = (y1_anc + y2_anc)/2.0

                #and compute the resulting regression outputs
                dx = cx - cxa
                dy = cy - cya
                tx = dx / (x2_anc - x1_anc)
                ty = dy / (y2_anc - y1_anc)
                tw = np.log((boxCoords[boxIdx][2] - boxCoords[boxIdx][0]) / (x2_anc - x1_anc))
                th = np.log((boxCoords[boxIdx][3] - boxCoords[boxIdx][1]) / (y2_anc - y1_anc))


                #if this anchor is close to the bounding box center ... go ahead
                #and assign it to the bounding box
                if np.abs(dx) <= RPN_STRIDE_WIDTH and np.abs(dy) <= RPN_STRIDE_HEIGHT :

                    anchorType = 'valid'
                    
                    anchorValid[jy, ix, 0] = 1
                    anchorOverlap[jy, ix, 0] = 1
                    anchorCoords[jy, ix, :] = (tx, ty, tw, th)

                    anchorClass[jy,ix,:] = [0,0,0,0]
                    anchorClass[jy,ix,boxClasses[boxIdx] ] = 1

                    # print("CLOSE NEOUGH", best_regr)


                #if the anchor slightly overlaps the bounding box
                # ... but is not centered enough to be assigned to that box
                #just make it not count for the loss
                if 0.3 < anchorIOU:                    
                    if anchorType != 'valid':
                        anchorType = 'no-loss'
            
            #and after searching through the boxes ... if nothing has touched
            #the anchor, set it to negative
            if anchorType == 'no-object':
                anchorValid[jy, ix, 0] = 1
                anchorOverlap[jy, ix, 0] = 0

            #and mark the ones that don't count towards the regression loss
            elif anchorType == 'no-loss':
                anchorValid[jy, ix, 0] = 0
                anchorOverlap[jy, ix, 0] = 0


    #find the negative and positive anchors
    positiveAnchors = np.where(np.logical_and(anchorOverlap[:, :, :] == 1, anchorValid[:, :, :] == 1))
    negativeAnchors = np.where(np.logical_and(anchorOverlap[:, :, :] == 0, anchorValid[:, :, :] == 1))

    #and cap the total number of loss anchors to 256
    positiveCount = len(positiveAnchors[0])
    negativeCount = 256 - positiveCount

    #if we have too many negative anchors ... nuke some (don't count toward loss)
    if len(negativeAnchors[0]) > negativeCount:
        extraAnchors = random.sample(range(len(negativeAnchors[0])), len(negativeAnchors[0]) - negativeCount)
        anchorValid[negativeAnchors[0][extraAnchors], negativeAnchors[1][extraAnchors], negativeAnchors[2][extraAnchors]] = 0

    
    #composite our final output textures
    anchorObjectness = np.concatenate([anchorValid, anchorOverlap], axis=2)
    anchorCoords = np.concatenate([np.repeat(anchorOverlap, 4, axis=2), anchorCoords], axis=2)
    anchorClass = np.concatenate([anchorOverlap, anchorClass], axis=2)
  
    return np.copy(anchorObjectness), np.copy(anchorCoords), np.copy(anchorClass)



#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Network definition
#################################################################################
#################################################################################
#################################################################################
#################################################################################


#network definition
def layerRPN(input_features):
 
    x = Conv2D(1024, (1, 1),  activation='relu', name='rpn_conv1')(input_features)
    x = Conv2D(512, (1, 1), activation='relu', name='rpn_conv2')(x)
    x = Dropout(0.15, name='rpn_dropout')(x)
    x = Conv2D(512, (1, 1),  activation='relu', name='rpn_conv3')(x)
    x = Conv2D(2048, (1, 1),  activation='relu', name='rpn_conv4')(x)

    objectness = Conv2D(1, (1, 1), activation='sigmoid', name='rpn_out_objectness')(x)
    boxcoords = Conv2D(4, (1, 1), activation='linear', name='rpn_out_boxcoords')(x)

    return [objectness, boxcoords]

def lossRPNRegression():

    #y_true[:,:,:,:4] is either all 1 if we should count this anchor and 0 otherwise

    #y_true[:,:,:,4:] is the regression targets

    def _lossRPNRegression(y_true, y_pred):
        
        diff = y_true[:,:,:,4:] - y_pred
        absDiff = K.abs(diff)

        isClose = K.cast(K.less_equal(absDiff, 1.0), tf.float32)

        return K.sum( y_true[:,:,:,:4] * (isClose * (0.5 * diff * diff) + (1 - isClose) * (absDiff - 0.5))) / K.sum(epsilon + y_true[:,:,:,:4])

    return _lossRPNRegression


def lossRPNObjectness():
  
    #y_true[:,:,:,:1] ... whether this anchor is should count here
    #y_true[:,:,:,1:] ... the 0/1 positive negative flag of the anchor

    def _lossRPNObjectness(y_true, y_pred):
            
            dx = y_pred[:,:,:,:] - y_true[:,:,:,1:]
            mask = y_true[:,:,:,:1]
            value = y_true[:,:,:,1:]
            
            #weighted means-square error
            weight = mask * ( value * 0.95 + (1-value) * 0.05)            
            return  K.sum(weight * dx * dx) / K.sum(epsilon + y_true[:,:,:,:1])

    return _lossRPNObjectness


#anchor classIdx layer
def layerClassification(baseLayers, inputObjectness, inputCoords):
  
    pooling = ROIPoolingLayer()([baseLayers, inputCoords])
    
    x = Conv2D(256, (1, 1), padding='same', activation='relu',  name='class_conv1')(pooling)
    x = Dropout(0.15, name='class_dropout')(x)
    x = Conv2D(64, (1, 1), padding='same', activation='relu',  name='class_conv2')(x)

    outputClass = Conv2D(4, (1, 1),  activation='softmax',  name='class_out')(x)

    return [inputObjectness, inputCoords,   outputClass]

#CCE loss for classification
def lossClassification():

    def _lossClassification(y_true, y_pred):
            
            mask = y_true[:, :, :, 0] #whether this was associated with a valid anchor
            value = y_true[:, :, :, 1:] #the one-hot class
            
            error = categorical_crossentropy(y_pred, value) * mask

            return  K.mean(error)
            
    return _lossClassification



#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Image Loaders
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################


##########################################################
# Scale smallest dimension to the reference size
##########################################################

#where we load our images from 
TRAIN_DIR = "augmented_images"
SHIPS_DIR = "augmented_ships"
AIRCRAFT_DIR = "augmented_aircraft"
PEOPLE_DIR = "augmented_people"
# TRAIN_DIR = "images"
# SHIPS_DIR = "ships"
# AIRCRAFT_DIR = "aircraft"
# PEOPLE_DIR = "people"

#generator to produce training examples
def ImageGenerator():

    boxCoords = []
    boxClasses = []

    #loop through the annotation directories and load everything
    aircraft = []
    for aircraftFile in os.listdir(AIRCRAFT_DIR):
        aircraft.append(os.path.splitext(aircraftFile)[0])
    aircraft.sort()

    ships = []
    for shipFile in os.listdir(SHIPS_DIR):
        ships.append(os.path.splitext(shipFile)[0])
    ships.sort()

    people = []
    for peopleFile in os.listdir(PEOPLE_DIR):
        people.append(os.path.splitext(peopleFile)[0])
    people.sort()
    
    #and now go through the training images
    for imgFile in os.listdir(TRAIN_DIR):

        
        imgPath = os.path.join(TRAIN_DIR,imgFile) 

        #this timestamp is the key used to fetch all the associated annotations 
        #for each image
        imgTimestamp = os.path.splitext(imgFile)[0]

        
        for classIdx, classLabels in enumerate([aircraft, ships, people]):

            for label in classLabels:
                
                parts = label.split('_')

                #make sure that this label applies to THIS image
                if (imgTimestamp != parts[0]):
                    continue
                
                #and go ahead and add the box to the list
                x1 = int(int(parts[1])/1280*TARGET_IMAGE_SIZE_WIDTH)
                y1 = int(int(parts[2])/720*TARGET_IMAGE_SIZE_HEIGHT)
                x2 = int(int(parts[3])/1280*TARGET_IMAGE_SIZE_WIDTH)
                y2 = int(int(parts[4])/720*TARGET_IMAGE_SIZE_HEIGHT)
                boxCoords.append([x1,y1,x2,y2])
                boxClasses.append(classIdx + 1)
            
        #if we have any label boxes for this image ... return them
        #otherwise keep going
        if len(boxCoords) > 0:
           
            print("Loading ", imgPath)
            
            image = load_img(imgPath, target_size=(180, 320),interpolation='bicubic')            
            image = img_to_array(image)

            anchorObjectness, anchorCoords, anchorClass = boxesToAnchors( boxCoords, boxClasses)
            yield image, boxCoords, anchorObjectness,anchorCoords, anchorClass
            
        boxCoords = []
        boxClasses = []

    
       

#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Main program
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################

#don't use up all the memories
opts = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.9)
sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=opts))


#where we're saving this thing
modelFile  = r'model.hdf5'

#define the input 
inputImageShape = (None, None, 3)
imageInput = Input(shape=inputImageShape)

#load VGG16 and nuke the last two layers
model = VGG16(include_top=False, input_tensor=imageInput, pooling='avg')
model.layers.pop()
model.layers.pop()

#and dear lord we don't want to change the underlying weights
#with this little data to train on 
for l in model.layers:
    l.trainable = False
    
#and the two other processing layers
rpn = layerRPN(model.layers[-1].output)
classification = layerClassification(model.layers[-1].output, rpn[0], rpn[1])

#put everything together
optimizer = Adam()
totalModel = Model(imageInput,classification)
totalModel.summary()
totalModel.compile(optimizer=optimizer, loss=[lossRPNObjectness(), lossRPNRegression(),  lossClassification()])


#and so now we need training data
imageSource = ImageGenerator()


#compile all our input and target data
X_total = []
Yobjectness_total = []
Ycoords_total = []
Yclass_total = []

#keep count just for displaying
count = 0
while True:

    #get the next data from the generator
    count += 1
    try:
        img, boxCoords, anchorObjectness, anchorCoords, anchorClass = next(imageSource)
    except:
        break

    height,width = img.shape[:2]

    #################################################################################
    ## sanity check visualization during data loading
    #################################################################################

    #and create an augmented version of the source image so we can 
    #generate debug renders
    imgAug = img.copy().astype('uint8')
    imgAug = cv2.cvtColor(imgAug, cv2.COLOR_RGB2BGR) #fix TF RGB->BGR for opencv

    #and draw the GT boxes
    for box in boxCoords:        
        imgAug = cv2.rectangle(imgAug, (box[0],box[1]), (box[2],box[3]), (0,255,0), 1)

    #and draw the anchor positions    
    targetHeight, targetWidth = anchorObjectness.shape[:2]
    for jy in range(targetHeight):
        for ix in range(targetWidth):
            
            valid = anchorObjectness[jy,ix,0]
            overlap = anchorObjectness[jy,ix,1]

            #if this is a 'counted in loss' anchor
            if valid > 0:

                #base anchor corners
                x1_anc = int(RPN_STRIDE_WIDTH * (float(ix) ) )
                x2_anc = int(RPN_STRIDE_WIDTH * (float(ix) + 1) )           
                y1_anc = int(RPN_STRIDE_HEIGHT * (float(jy) ) )
                y2_anc = int(RPN_STRIDE_HEIGHT * (float(jy) + 1) )

                #if these belong to actual bounding boxes
                if overlap > 0:                               
                    
                    #mark up the debug render with the anchor highlighted in yellow
                    imgAug = cv2.rectangle(imgAug, (x1_anc,y1_anc), (x2_anc,y2_anc), (0,255,255), 1)

                    #load the coordinate regression targets and convert into pixel space
                    coords = anchorCoords[jy,ix,4:]                            
                    tx = coords[0]
                    ty = coords[1]
                    tw = coords[2]
                    th = coords[3]
                    
                    cx = (x1_anc + x2_anc)/2
                    cy = (y1_anc + y2_anc)/2
                    cw = (x2_anc - x1_anc)
                    ch = (y2_anc - y1_anc)

                    cx = cx + tx * cw
                    cy = cy + ty * ch
                    cw = np.exp(tw) * cw
                    ch = np.exp(th) * ch

                    nx1 = int(cx - cw/2)
                    nx2 = int(cx + cw/2)
                    ny1 = int(cy - ch/2)
                    ny2 = int(cy + ch/2) 

                    #draw the target bounding box (should exactly match the GT)
                    imgAug = cv2.rectangle(imgAug, (nx1 + 1,ny1 + 1), (nx2 - 1,ny2 - 1), (255,255,0), 1)
                    

                #otherwise mark it in the debug render as a negative anchor
                else:
                    imgAug = cv2.rectangle(imgAug, (x1_anc,y1_anc), (x2_anc,y2_anc), (0,0,255), 1)
 

    #and modify the source image for training dimensions
    imgFix = np.copy(img)
    imgFix = np.expand_dims(imgFix, axis=0)

    #same with the target textures
    anchorCoords = np.expand_dims(anchorCoords, axis=0).astype(np.float32)
    anchorObjectness = np.expand_dims(anchorObjectness, axis=0).astype(np.float32)
    anchorClass = np.expand_dims(anchorClass, axis=0).astype(np.float32)

    #and store the final result
    X_total.append(np.copy(imgFix))
    Yobjectness_total.append(np.copy(anchorObjectness))
    Ycoords_total.append(np.copy(anchorCoords))
    Yclass_total.append(np.copy(anchorClass))

    #and display what we're loading so we can check it visually
    cv2.imshow("testing", imgAug)
    cv2.waitKey(1)

#reshape our input data
X = np.vstack(X_total)
Y = [np.vstack(Yobjectness_total), np.vstack(Ycoords_total), np.vstack(Yclass_total)]

#and shuffle ... eh ... adds some fun to the process
numExamples = X.shape[0]
inds = [*range(numExamples)] 
random.shuffle(inds)
X = X[inds,:,:,:]
Y[0] = Y[0][inds,:,:,:]
Y[1] = Y[1][inds,:,:,:]
Y[2] = Y[2][inds,:,:,:]

#and a final sanity check 
print("Features shape:", X.shape)
print("Objectness shape:", Y[0].shape)
print("Coords shape:", Y[1].shape)
print("Class shape:", Y[2].shape)



#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
## Training 
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################

#where we'll write our masks used for confusion matrix calculations
METRICS_DIR = "metrics"
os.system("mkdir " + METRICS_DIR)


#a ... sort of pixel-wise confusion metric
class Metrics(keras.callbacks.Callback):

    def on_train_begin(self, logs={}):
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []

    def on_epoch_end(self, epoch, logs):

        #get our input/output data for this batch
        input_features = self.validation_data[0]
        target_objectness = self.validation_data[1]   
        target_boxes = self.validation_data[2]       
        target_classes = self.validation_data[3]

        #how many validation examples do we have
        num_validation = input_features.shape[0]

        #go ahead and predict on these
        pred_output = self.model.predict_on_batch(input_features)      

        #the total confusion matrix we'll update 
        confusion_matrix = np.zeros((2,2,3), dtype='float32')

        #for each example
        for b in range(num_validation):

            #reshape the target data into a series of 2D arrays
            objectness = target_objectness[b,:,:,1]        
            boxes = target_boxes[b,:,:,:]        
            classes = target_classes[b,:,:,2:5]
            classes = classes[:,:,0]  + classes[:,:,1]*2 + classes[:,:,2]*3            

        
            h,w = objectness.shape[:2]

            #we're going to draw the GT bounding boxes in anchor space
            target_detections = np.zeros((h,w,3), dtype='uint8')

            #draw the boxes in anchor space for each class
            for c in range(3):

                inds = np.where(classes == c+1)
                for i in range(len(inds[0])):

                    y = inds[0][i]
                    x = inds[1][i]                

                    x1_anc = float(x)
                    x2_anc = float(x + 1)
                    y1_anc = float(y)
                    y2_anc = float(y + 1) 
                
                    tx = boxes[y,x,4+0]
                    ty = boxes[y,x,4+1]
                    tw = boxes[y,x,4+2]
                    th = boxes[y,x,4+3]

                    cx = (x1_anc + x2_anc)/2
                    cy = (y1_anc + y2_anc)/2
                    cw = (x2_anc - x1_anc)
                    ch = (y2_anc - y1_anc)

                    cx = cx + tx * cw
                    cy = cy + ty * ch
                    cw = np.exp(tw) * cw
                    ch = np.exp(th) * ch

                    nx1 = int(np.max([np.floor(cx - cw/2),0]))
                    nx2 = int(np.min([np.ceil(cx + cw/2),w-1]))
                    ny1 = int(np.max([np.floor(cy - ch/2),0]))
                    ny2 = int(np.min([np.ceil(cy + ch/2),h-1]))

                    target_detections[ny1:ny2,nx1:nx2,c] = 1

            #now do the same thing except for the model output       
            objectness = pred_output[0][b,:,:,0]
            boxes = pred_output[1][b,:,:,:]
            classes = pred_output[2][b,:,:,:]
            classes = classes[:,:,1]  + classes[:,:,2]*2 + classes[:,:,3]*3            
                    
            pred_detections = np.zeros((h,w,3), dtype='uint8')
            for c in range(3):

                inds = np.where(classes == c+1)
                for i in range(len(inds[0])):

                    y = inds[0][i]
                    x = inds[1][i]                

                    x1_anc = float(x)
                    x2_anc = float(x + 1)
                    y1_anc = float(y)
                    y2_anc = float(y + 1) 
                
                    tx = boxes[y,x,0]
                    ty = boxes[y,x,1]
                    tw = boxes[y,x,2]
                    th = boxes[y,x,3]

                    cx = (x1_anc + x2_anc)/2
                    cy = (y1_anc + y2_anc)/2
                    cw = (x2_anc - x1_anc)
                    ch = (y2_anc - y1_anc)

                    cx = cx + tx * cw
                    cy = cy + ty * ch
                    cw = np.exp(tw) * cw
                    ch = np.exp(th) * ch

                    nx1 = int(np.max([np.floor(cx - cw/2),0]))
                    nx2 = int(np.min([np.ceil(cx + cw/2),w-1]))
                    ny1 = int(np.max([np.floor(cy - ch/2),0]))
                    ny2 = int(np.min([np.ceil(cy + ch/2),h-1]))

                    pred_detections[ny1:ny2,nx1:nx2,c] = 1


            #and now do a pixel-wise counting to update
            #the confusion matrix
            for c in range(3):
                
                true_positives = target_detections[:,:,c] * pred_detections[:,:,c]
                true_negatives = (1-target_detections[:,:,c]) * (1-pred_detections[:,:,c])
                false_positives = (1-target_detections[:,:,c]) * (pred_detections[:,:,c])
                false_negatives = (target_detections[:,:,c]) * (1-pred_detections[:,:,c])

                confusion_matrix[0,0,c] += np.sum(true_positives)
                confusion_matrix[0,1,c] += np.sum(false_positives)
                confusion_matrix[1,0,c] += np.sum(false_negatives)
                confusion_matrix[1,1,c] += np.sum(true_negatives)

         
            #save some of these images so we can visually inspect that things make sense
            target_source = input_features[b,:,:,1]
            minVal = np.min(target_source)
            maxVal = np.max(target_source)
            target_source = (target_source - minVal) / (maxVal - minVal) *255       
            cv2.imwrite(os.path.join(METRICS_DIR, str(b) + "source.bmp"), target_source.astype('uint8'))
            target_detections = cv2.resize(target_detections, (320,180), interpolation=cv2.INTER_NEAREST)
            cv2.imwrite(os.path.join(METRICS_DIR, str(b) + "target.bmp"), target_detections.astype('uint8')*255)
            pred_detections = cv2.resize(pred_detections, (320,180), interpolation=cv2.INTER_NEAREST)
            cv2.imwrite(os.path.join(METRICS_DIR, str(b) + "prediction.bmp"), pred_detections.astype('uint8')*255)
        
        #and finally print the confusion matrix (just need it for the presentation)
        confusion_matrix = confusion_matrix.astype('int')
        for c in range(3): 
            print("Confusion for", c, confusion_matrix[:,:,c])
    
        return


#finally, finally, finally ... train
confusion_metrics = Metrics()
X = preprocess_input(X)
checkpointer = ModelCheckpoint(filepath=modelFile, verbose=1, save_best_only=True)
totalModel.fit(X,Y, validation_split=0.1,batch_size=32, nb_epoch=120, verbose=1, callbacks=[checkpointer, confusion_metrics],shuffle=True) 
