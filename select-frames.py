import cv2
import numpy as np
import os


#utility to format a ms-accuate timestamp string
def timeString(ms):
    return '{:013}'.format(int(ms))

#make sure we have some place to store selected images
os.system("mkdir images")

#open the file for capture
cap = cv2.VideoCapture(r"train.mp4")

#if we want to start labelling at some point in the video can fast foward here
cap.set(cv2.CAP_PROP_POS_MSEC, 185440)

#use the video timestamp to mark the frame
timestamp = cap.get(cv2.CAP_PROP_POS_MSEC)
print("Current timestamp", timestamp)

#read the initial frame to get statistics
_, rawFrame = cap.read()
frameHeight, frameWidth = rawFrame.shape[:2]
frameCount = 0

while True:
    
    #mark the frame with instructions and display
    frame = rawFrame.copy()
    frame = cv2.putText(frame, timeString(timestamp), (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    frame = cv2.putText(frame, "T: select frame", (10,frameHeight-10), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    frame = cv2.putText(frame, "N: advance to next frame", (10,frameHeight-40), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    frame = cv2.putText(frame, "Q: quit", (10,frameHeight-70), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,0,0), 2, cv2.LINE_4)
    cv2.imshow("Select Frames to Label", frame)

    #let the user respond
    key = cv2.waitKey(0) & 0xFF

    #quit
    if key == ord("q"):
        exit()

    #save frame for labelling
    elif key == ord("t"): 
        cv2.imwrite("images\\" + timeString(timestamp) + ".bmp", rawFrame)

    #advance to next frame
    elif key == ord("n") or frameCount == 0:
        _, rawFrame = cap.read()
    
    #and read the next frame and update the timestamp
    timestamp = cap.get(cv2.CAP_PROP_POS_MSEC)
    print("Current timestamp", timestamp)
    frameCount += 1
    
    
    
        
         