import cv2
import numpy as np
import os
import random

#we'll keep a raw image frame and then we'll also 
rawFrame = np.zeros((10,10,3), dtype=np.uint8)

#want something to draw on
augFrame = np.zeros((10,10,3), dtype=np.uint8)

#source and target directories
IMAGES_DIRECTORY = "images"
AIRCRAFT_DIRECTORY = "aircraft"
SHIPS_DIRECTORY = "ships"
PEOPLE_DIRECTORY = "people"

IMAGES_AUG_DIRECTORY = "augmented_images"
AIRCRAFT_AUG_DIRECTORY = "augmented_aircraft"
SHIPS_AUG_DIRECTORY = "augmented_ships"
PEOPLE_AUG_DIRECTORY = "augmented_people"

#make sure we have all the directories ... and clean out
#and existing augmentation
os.system("mkdir " + IMAGES_AUG_DIRECTORY)
os.system("mkdir " + AIRCRAFT_AUG_DIRECTORY)
os.system("mkdir " + SHIPS_AUG_DIRECTORY)
os.system("mkdir " + PEOPLE_AUG_DIRECTORY)
os.system("erase /Q " + AIRCRAFT_AUG_DIRECTORY + "\\*")
os.system("erase /Q " + SHIPS_AUG_DIRECTORY + "\\*")
os.system("erase /Q " + PEOPLE_AUG_DIRECTORY + "\\*")

#utility to get a time string from a timestamp
def timeString(ms):
    return '{:013}'.format(int(ms))


#load all the annotations FOR A SPECIFIC IMAGE timestamp
def loadAnnotations(directory, timestamp):


    annotations = []

    for imgFile in os.listdir(directory):

        baseName = os.path.splitext(imgFile)[0]
        annotationParts = baseName.split('_')

        if (timestamp != annotationParts[0]):
            continue
                
        x1 = int(int(annotationParts[1]))
        y1 = int(int(annotationParts[2]))
        x2 = int(int(annotationParts[3]))
        y2 = int(int(annotationParts[4]))
        
        annotations.append((x1,y1,x2-x1,y2-y1))

    return annotations


#when we flip the image left-right, also need to flip the annotations
def flipAnnotations(input):

    output = []
    for r in input:
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]     

        a = 1280 - x1
        b = 1280 - x2

        x1 = b
        x2 = a

        output.append((x1,y1,x2-x1,y2-y1))

    return output

#crop an image to subcoordinates ... then
#resize to the original dimensions
def crop(input, fx1, fy1, fx2, fy2): #fx1, fy1, etc are in image normalized coordinates [0-1]

    x1 = int(fx1*1280)
    y1 = int(fy1*720)
    x2 = int(fx2*1280)
    y2 = int(fy2*720)

    output = input[y1:y2,x1:x2,:]
    output = cv2.resize(output,(1280,720))
    return output

#just like the image ... when we crop we need to adjust existing annotation coordinates
def cropAnnotations(input, fx1, fy1, fx2, fy2):

    output = []
    for r in input:
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]      

        print("original", x1,y1,x2,y2)
        x1 = (x1 / 1280 - fx1) / (fx2 - fx1)
        y1 = (y1 / 720 - fy1) / (fy2 - fy1)
        x2 = (x2 / 1280 - fx1) / (fx2 - fx1)
        y2 = (y2 / 720 - fy1)/ (fy2 - fy1)

        if (x1 < 0.9 and y1 < 0.9 and x2 > 0.1 and y2 > 0.1 ):

            x1 = max(0,x1)
            y1 = max(0,y1)
            x2 = min(1,x2)
            y2 = min(1, y2)
            x1 = int(x1  * 1280)
            y1 = int(y1  * 720)
            x2 = int(x2  * 1280)            
            y2 = int(y2  * 720)
            print("post crop", x1,y1,x2,y2)
        
            output.append((x1,y1,x2-x1,y2-y1))

    return output


#and save an annotation at a given timestamp
def saveImages(timestamp, rawFrame, aircraft, ships, people):


    cv2.imwrite(os.path.join(IMAGES_AUG_DIRECTORY,timestamp + ".bmp") , rawFrame)

    for i,r in enumerate(aircraft):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]
        print(x1,y1,x2,y2)
        cv2.imwrite(os.path.join(AIRCRAFT_AUG_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)

    for i,r in enumerate(ships):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]
        print(x1,y1,x2,y2)
        cv2.imwrite(os.path.join(SHIPS_AUG_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)

    for i,r in enumerate(people):
        x1 = r[0]
        y1 = r[1]
        x2 = r[0] + r[2]
        y2 = r[1] + r[3]        
        img = rawFrame[y1:y2,x1:x2,:]
        print(x1,y1,x2,y2)
        cv2.imwrite(os.path.join(PEOPLE_AUG_DIRECTORY, timestamp + "_" + str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".bmp"), img)


#load all the images we have
timestamps = []
for imgFile in os.listdir(IMAGES_DIRECTORY):

    baseName = os.path.splitext(imgFile)[0]
    timestamps.append(baseName)

timestamps.sort()

#for all these images ... load and augment their annotations
for timestamp in timestamps:

    rawFrame = cv2.imread(os.path.join(IMAGES_DIRECTORY, timestamp + ".bmp"))
    aircraft = loadAnnotations(AIRCRAFT_DIRECTORY, timestamp)
    ships = loadAnnotations(SHIPS_DIRECTORY, timestamp)
    people = loadAnnotations(PEOPLE_DIRECTORY, timestamp)

    #save the original annotations
    saveImages(timestamp, rawFrame, aircraft, ships, people)
    
    #for augmentations we have to use unique timestamps ... since their
    #in ms we can increment by 1 without collision with the next frame
    #next frame would be +30ms at least
    newTimestamp = int(timestamp) + 1
    
    #iterate twice ... we'll do one pass flipped Left->Right
    #and another pass unaltered
    for j in range(2):    
        
        #and generate three augmentations
        for i in range(3):
            timestampAug = timeString(newTimestamp)
            newTimestamp += 1
                
            fx1 = random.random() * 0.2
            fy1 = random.random() * 0.2
            fx2 = 0.8 + random.random() * 0.2
            fy2 = 0.8 + random.random() * 0.2
            print(fx1,fy1,fx2,fy2)

            orawFrame = crop(rawFrame,fx1,fy1,fx2,fy2)
            oaircraft = cropAnnotations(aircraft,fx1,fy1,fx2,fy2)
            oships = cropAnnotations(ships,fx1,fy1,fx2,fy2)
            opeople = cropAnnotations(people,fx1,fy1,fx2,fy2)
        
            if (len(oaircraft) > 0 or len(oships) > 0 or len(opeople) > 0):    
                saveImages(timestampAug, orawFrame, oaircraft, oships, opeople)
    
        rawFrame = cv2.flip(rawFrame, 1)
        aircraft = flipAnnotations(aircraft)
        ships = flipAnnotations(ships)
        people = flipAnnotations(people)
    
        
         