**DeepMilitary: Aircraft, Ships, and People Detection using TF/Keras/FasterRCNN**

NOTE:This was developed for Windows and may include a few Windows-specific system commands and path problems if run in another environment

Here's a link to a [presentation summarizing this work](https://docs.google.com/presentation/d/12AjvCxaN_h4pTfw2FcqYED4v5nCR1fEO9DVK1ow2ceQ/edit?usp=sharing)


And a [video of detections with non-max suppression](https://youtu.be/jN1a8MYrwx4)

And a [video of detections WITHOUT non-max suppression](https://youtu.be/MJxbBCoNy4U)




---

## Overview

This repo includes the data labelling pipeline as well as the code to train and test the network. The order of execution goes like this

1. select-frames.py (Choose the image frames to label from a video)
2. label-images.py (Apply labels)
3. augment-examples.py (Augment the training data)
4. train-network.py (Train network)
5. test-network.py (Perform detection on a new video)

Labelled training data has been included in this repo

---

## Frame selection (optional for testing)

** NOTE: This step does not need to be performed to test the network, since training data is already provided **

First, copy a 720p video you want to label into the clone directory and rename it to input.mp4

Keyboard command instrctions are provided on screen

Run select-frames.py


---

## Image labelling (optional for testing)

** NOTE: This step does not need to be performed to test the network, since training data is already provided **

Keyboard command instrctions are provided on screen

Run label-images.py

---

## Augmenting training data

This will take all the labels in the ships/aircraft/people directory and create new examples and place them in the augmented_ships/augmented_aircraft/augmented_people directory

Run augment-examples.py


---

## Training the network

** NOTE: This requres TensorFlow 2.0 to be installed **

Run train-network.py


---

## Testing the network

** NOTE: This requres TensorFlow 2.0 to be installed **

To test the network, copy a new 720p video into the clone directory and rename it test.mp4

Run test-network.py


